{ pkgs ? import <nixpkgs> { }
, isDarwin ? builtins.currentSystem == "x86_64-darwin"
}:
let stdenv = if isDarwin then pkgs.clangStdenv else pkgs.stdenv;
in
stdenv.mkDerivation rec {
  name = "catgirl";
  version = "1.3";
  src = pkgs.fetchurl {
    url = "https://git.causal.agency/catgirl/snapshot/catgirl-${version}.tar.gz";
    sha256 = "vgDqC9PIqVgseS0nZ7waZx4F3/g5oka1M3l3QvzajOw=";
  };
  nativeBuildInputs = with pkgs; [
    pkg-config
  ];
  buildInputs = with pkgs; [
    ncurses
    ncurses.dev
    libressl
    libressl.dev
  ];
  configurePhase = "./configure --prefix=$out";
  buildPhase = ''
    make all
    ${if isDarwin then "make scripts/sandman" else ""}
  '';
  installPhase = ''
    mkdir -p $out
    make install
    ${if isDarwin then "make install-sandman" else ""}
  '';
}
